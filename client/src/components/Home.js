import React from "react";
import NavBar from "../layouts/NavBar";

function Home(props) {
    return (
        <>
            <NavBar/>
            <h1>Home</h1>
        </>
    );
}

export default Home;