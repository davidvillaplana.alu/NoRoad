import React from "react";
import NavBar from "../layouts/NavBar";
import RegisterForm from "../layouts/RegisterForm";

function Register(props) {
    return (
        <>
            <NavBar/>
            <RegisterForm/>
        </>
    );
}

export default Register;